# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is to find users based on their location 

### How do I run it? ###

To run you need to build it first by taping:
docker-compose up --build 

and then to run you need to type:
docker-compose up

### Contributors  ###
Pranay Mistri - Dockerfile.mysql
Natalia Christofi - Dockerfile.app

Both authors contributed for the yaml file and the rest.
